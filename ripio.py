import numpy
import os
def load(file:str, shape, dtype=numpy.float32):
    """Load a binary float file, returns MxNxN tensor"""
    if not os.path.isfile(file):
        raise FileNotFoundError(f"{file} either does not esist or is not a file... Try again.")
    
    raw = numpy.fromfile(file, dtype=dtype)

    if not len(raw) == numpy.prod(shape):
        raise Exception(f"Expected file to be {shape[0]}x{shape[1]}x{shape[2]}={numpy.prod(shape)} elements, but was {len(raw)} elements...")
    
    reshaped = raw.reshape(shape, order='c')
    return reshaped

def save(file:str, data:numpy.ndarray, dtype=numpy.float32):
    """Save data as a c-order binary file
    
    Index 0: Projection index
    index 1: axial index
    index 2: lateral index
    """
    data = numpy.ascontiguousarray(data, dtype=dtype)
    data.tofile(file)