import numpy as np
from ripio import load
from scipy.interpolate import RegularGridInterpolator as RGI

def rotate_interp(pet_data, n_angles: int, ct_flg: bool = False, ct_data : np.ndarray = np.zeros((5,5))):
    '''
    Function to rotate and interpolate data in order, accepts ct data also as necessary

    Inputs
    ---------
    pet_data : data matrix (n_slices x 128 x 128)
    n_angles : angles to rotate by, over a total of 360 degrees

    ct_flg : int (0 or 1) specifies ct rotation requirement
    ct_data : np.ndarray (n_slices x 512 x 512)

    Returns:
    ---------
    rotated_slices = n_slices x 128 x 128 x n_angles
    '''

    # define angles of rotation
    theta_step = (2*np.pi)/n_angles
    angles = np.arange(0,(2*np.pi),theta_step)

    # define a matrix containing each (coordinate point) for each intensity value
    x_mat, y_mat, z_mat = [*pet_data.shape]

    # center the y_mat and z_mat ranges
    y_range = range(y_mat) - np.ones(y_mat) * y_mat/2
    z_range = range(z_mat) - np.ones(z_mat) * z_mat/2

    big_x, big_y, big_z = np.meshgrid(range(x_mat), y_range, z_range, indexing='ij')
    coor_mat = np.array([big_x.flatten(), big_y.flatten(), big_z.flatten()])

    interp_pet = RGI((range(x_mat), y_range, z_range), pet_data, bounds_error=False, fill_value=0)
    rotated_pets = np.zeros([*pet_data.shape, n_angles])

    # addresses the need for ct depth weighting
    if ct_flg:
        scale = int(ct_data.shape[1]/pet_data.shape[1])
        ct_data = ct_data[:, ::scale, ::scale]

        interp_ct = RGI((range(x_mat), y_range, z_range), ct_data, bounds_error=False, fill_value=0)
        rotated_cts = np.zeros((rotated_pets.shape))

    for i in range(n_angles):
        # define rotational matrix, around x
        rot_mat = [
            [1,0,0],
            [0, np.cos(angles[i]),-np.sin(angles[i])],
            [0, np.sin(angles[i]),np.cos(angles[i])],
         ]
        
        # multiply rot mat by coordinate matrix
        coord_guy = rot_mat @ coor_mat

        # apply interp to the new slice
        pet_sliceguy = np.array(interp_pet(np.transpose(coord_guy))).reshape((pet_data.shape))
        rotated_pets[:,:,:,i] = pet_sliceguy

        if ct_flg:
            ct_sliceguy = np.array(interp_ct(np.transpose(coord_guy))).reshape((pet_data.shape))
            rotated_cts[:,:,:,i] = ct_sliceguy

    if ct_flg:
        return rotated_pets, rotated_cts
    
    else:
        return rotated_pets