import numpy as np
import tkinter as tk
from tkinter import filedialog
from pathlib import Path
import ripio as rio

global __FILE_NAMES__, __N_SLICE__, __N_PROJ__, __CT_SIZE__, __PET_SIZE__, __MIP_SIZE__, __FILESEARCH_ROOT__
__FILE_NAMES__ = []
__N_SLICE__ = 47
__N_PROJ__ = 1
__CT_SIZE__ = [1,1,2.5]
__PET_SIZE__ = [4,4,2.5]
__MIP_SIZE__ = [4,2.5,__N_PROJ__]
__FILESEARCH_ROOT__ = "/"

class FileButton(tk.Button):
    __IND_COUNT__ = 0
    def __init__(self, parent, label, load=True, func = None):
        if func is None:
            tk.Button.__init__(self, parent, text=label, command=lambda:self.getfile())
        else:
            self.func = func
            tk.Button.__init__(self, parent, text=label, command=lambda:self.func())

        global __FILE_NAMES__
        self.id = FileButton.__IND_COUNT__
        self.load = load
        __FILE_NAMES__.append('')
        FileButton.__IND_COUNT__ += 1

    def getfile(self):
        global __FILE_NAMES__, __FILESEARCH_ROOT__
        if self.load:
            __FILE_NAMES__[self.id] = filedialog.askopenfilename(initialdir=__FILESEARCH_ROOT__)
        else:
            __FILE_NAMES__[self.id] = filedialog.asksaveasfilename(initialdir=__FILESEARCH_ROOT__)
        if type(__FILE_NAMES__[self.id]) is not str or __FILE_NAMES__[self.id] == '':
            __FILE_NAMES__[self.id] == ''
        else:
            __FILESEARCH_ROOT__ = str(Path(__FILE_NAMES__[self.id]).parent) + '/'
        print(__FILE_NAMES__[self.id])

class IOParams(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        # Generate all file ui elements
        self.paraml0 = tk.Label(self, text="User Input Parameters")
        self.paraml1 = tk.Label(self, text="# Slices")
        self.paraml2 = tk.Label(self, text="# Projections")
        self.in1 = tk.Text(self, height=1, width=5)
        self.in1.insert(tk.INSERT, f"{__N_SLICE__}")
        self.in2 = tk.Text(self, height=1, width=5)
        self.in2.insert(tk.INSERT, f"{__N_PROJ__}")
        self.parbtn = tk.Button(self, text='Accept', command=lambda:self.__getsliceproj())

        # position all elements within this widget
        self.paraml0.grid(row=0, column=0, columnspan=2, pady=2)
        self.paraml1.grid(row=1, column=0, pady=2)
        self.paraml2.grid(row=1, column=1, pady=2)
        self.in1.grid(row=2, column=0, pady=2)
        self.in2.grid(row=2, column=1, pady=2)
        self.parbtn.grid(row=3, column=0, columnspan=2, pady=2)

    def __getsliceproj(self):
        global __N_SLICE__, __N_PROJ__, __MIP_SIZE__
        try:
            __N_SLICE__ = int(self.in1.get('1.0', 'end-1c')) 
        except Exception as e: 
            print(f"Unable to convert input string {self.in1.get('1.0', 'end-1c')} to an integer")
        try:
            __N_PROJ__ = int(self.in2.get('1.0', 'end-1c')) 
        except Exception as e: 
            print(f"Unable to convert input string {self.in2.get('1.0', 'end')} to an integer")

        __MIP_SIZE__[2] = 2*np.pi/__N_PROJ__

class DispParams(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        global __PET_SIZE__, __CT_SIZE__, __MIP_SIZE__
        # Generate PET size inputs
        titles = [
            "PET Voxel Dimensions [mm]",
            "CT Voxel Dimensions [mm]",
            "MIP Pixel Dimensions [mm]"
        ]
        self.inputs = []
        buffers = [__PET_SIZE__, __CT_SIZE__, __MIP_SIZE__]
        for ind in range(3):
            frame = tk.Frame(self)
            suplab = tk.Label(frame, text=titles[ind])
            dxl = tk.Label(frame, text="dx: ")
            dyl = tk.Label(frame, text="dy: ")
            dzl = tk.Label(frame, text="dz: ")

            dx = tk.Text(frame, height=1, width=5)
            dx.insert(tk.INSERT, f"{buffers[ind][0]}")
            dy = tk.Text(frame, height=1, width=5)
            dy.insert(tk.INSERT, f"{buffers[ind][1]}")
            dz = tk.Text(frame, height=1, width=5)
            dz.insert(tk.INSERT, f"{buffers[ind][2]}")

            suplab.grid(row=0, column=0, columnspan=2)
            dxl.grid(row=1, column=0)
            dx.grid(row=1, column=1)
            dyl.grid(row=2, column=0)
            dy.grid(row=2, column=1)
            dzl.grid(row=3, column=0)
            dz.grid(row=3, column=1)
            frame.grid(row=ind)

            self.inputs.append([dx, dy, dz])

        self.parbtn = tk.Button(self, text='Accept', command=lambda:self.__getdims())
        self.parbtn.grid(row=3, column=0, columnspan=2, pady=2)

    def __getdims(self):
        global __PET_SIZE__, __CT_SIZE__, __MIP_SIZE__
        buffers = [__PET_SIZE__, __CT_SIZE__, __MIP_SIZE__]
        vals = np.zeros((3,3), dtype=int)
        for ind in range(3):
            for indx in range(3):
                try:
                    vals[ind, indx] = float(self.inputs[ind][indx].get('1.0', 'end-1c'))
                    buffers[ind][indx] = vals[ind, indx]
                except Exception as e:
                    print(f"Something went wrong {ind}{indx}")
                

class UIInputSelect(tk.Frame):
    def __init__(self, parent, acceptfunc):
        tk.Frame.__init__(self, parent)

        # instantiate sub widgets
        self.ctfilebtn = FileButton(self, label='CT File')
        self.petfilebtn = FileButton(self, label='PET File')
        self.sellabel = tk.Label(self, text='File Selection')
        self.acbtn = tk.Button(self, text='Accept', command=acceptfunc)

        # position widgets
        self.sellabel.grid(row=0, pady=2)
        self.ctfilebtn.grid(row=1, pady=2)
        self.petfilebtn.grid(row=2, pady=2)
        self.acbtn.grid(row=4, pady=2)

class UIOutputSelect(tk.Frame):
    def __init__(self, parent, func):
        tk.Frame.__init__(self, parent)
        self.sellabel = tk.Label(self, text='MIP File Selection')
        self.loadbtn = FileButton(self, label='Load Existing MIP')
        self.makebtn = FileButton(self, label='Generate MIP File', load=False)
        self.procbtn = tk.Button(self, text='Process', command=func)

        # position widgets
        self.sellabel.grid(row=0, pady=2)
        self.loadbtn.grid(row=1, pady=2)
        self.makebtn.grid(row=2, pady=2)
        self.procbtn.grid(row=3, pady=2)

if __name__ == '__main__':
    print("In UIElements...")
    root = tk.Tk()
    root.title("UIElements Test")

    # Define UI widgets
    paramsel = IOParams(root)
    filesel = UIInputSelect(root, lambda:print("We made it bb"))

    # Define positioning of UI widgets
    paramsel.grid(row=0)
    filesel.grid(row=1)

    root.mainloop()