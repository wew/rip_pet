import gui
import ripio as rio
from depth_weight import *
from rotate_interp import rotate_interp
import sys

if __name__ == '__main__':
    # load command line arguments [arg[0]] is "launch.py"
    args = sys.argv

    # if no arguments other than launch.py, launch the GUI
    if len(args) == 1:
        gui.root.mainloop()

    # If given the number of slices, a CT file, and a PEt file
    elif len(args) == 5:
        # load and convert the command line arguments
        nslice = int(args[1])
        nangles = int(args[2])
        ctdata = args[3]
        petdata = args[4]

        # load the files
        ct_data = rio.load(ctdata, (nslice,512,512))
        pet_data = rio.load(petdata, (nslice,128,128))

        woct_mip = define_weight_mip(pet_data, nangles, 0.8)
        wct_mip = ct_weight_mip(pet_data, ct_data, nangles, 0.8)

        from pathlib import Path
        file_woct = Path(Path(ctdata).parent, "woct.mip")
        rio.save(file_woct, woct_mip)

        file_wct = Path(Path(ctdata).parent, "wct.mip")
        rio.save(file_wct, wct_mip)

        print(woct_mip.shape)
        print(wct_mip.shape)
        
        plt.figure()
        plt.imshow(woct_mip[0,:,:])
        plt.colorbar()
        plt.title("No CT weighting")
        
        plt.figure()
        plt.imshow(wct_mip[0,:,:])
        plt.colorbar()
        plt.title("CT weighting")
        plt.show()
    else:
        print("Too many or not enough arguments....")