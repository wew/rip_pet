import numpy as np
import tkinter as tk

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
# Implement the default Matplotlib key bindings.
from matplotlib.figure import Figure

# shared global parameters
global __GRAPH_I_SEL__, __GRAPH_DATA__
__GRAPH_I_SEL__ = []
__GRAPH_DATA__ = []

# iamge plot frame object
class ImPlot(tk.Frame):
    __IND_COUNT__ = 0
    """Updatable figure object that can be instantiated with a parent object"""
    def __init__(self, parent, size, dpi, xlabel, ylabel, clim, cmap, extent, title=""):
        tk.Frame.__init__(self, parent)
        self.id = ImPlot.__IND_COUNT__
        ImPlot.__IND_COUNT__ += 1
        self.parent = parent
        self.fig = Figure(size, dpi)
        self.ax = self.fig.add_axes([0.125,0.125,0.8,0.8])
        self.canvas = FigureCanvasTkAgg(self.fig, master=self)
        self.widget = self.canvas.get_tk_widget()
        self.canvas.draw()
        self.widget.pack()
        self.clim = clim
        self.extent = extent
        self.cmap = cmap
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.title = title
        __GRAPH_I_SEL__.append(int(0))
        temp = clim[0]*np.ones((1, 100, 100))
        temp[:,2::4,2::4] = clim[1]
        __GRAPH_DATA__.append(temp)
        self.update()

    def plotnew(self, data):
        ax = self.ax
        canvas = self.canvas
        ax.imshow(data, vmin=self.clim[0], vmax=self.clim[1], cmap=self.cmap, extent=self.extent)
        ax.set_xlabel(self.xlabel)
        ax.set_ylabel(self.ylabel)
        ax.set_title(self.title)
        canvas.draw()
    
    def update(self):
        global __GRAPH_I_SEL__, __GRAPH_DATA__
        self.plotnew(__GRAPH_DATA__[self.id][:,:,__GRAPH_I_SEL__[self.id]])
