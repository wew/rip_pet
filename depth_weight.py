import numpy as np
from ripio import load
from rotate_interp import rotate_interp
import matplotlib.pyplot as plt
import time

def ct_weight_mip(pet_data, ct_data, n_angles, tau : float = 0.5):
    '''
    Function to create depth-weight profile based off of CT image

    Inputs
    ---------
    ct_data : data matrix (n_angles x ct_n_slices x 512 x 512)
    pet_data : data matrix (n_angles x n_slices x 128 x 128)
    tau : float value used to construct depth weighting curve
    n_angles : int

    ** we assume that ct is composed of rotated datasets already **

    Returns:
    ---------
    mip_slice = n_angles x n_slices x 128 (2D image for each rotation)
    '''

    # rotate and interp both the ct and pet data
    pet_total, ct_total = rotate_interp(pet_data, n_angles, True, ct_data)
    print(pet_total.shape)

    n_angles = pet_total.shape[3]
    n_slices = pet_total.shape[0]

    total_mip = np.zeros((n_angles, n_slices, pet_total.shape[1]))

    for i in range(n_angles):

        # find where the average of ct slices starts to be greater than a specific value
        win = 10
        end = ct_total.shape[1]
        max_ct = np.max(ct_total[:,win:end - win,win:end - win], axis = (0,2))

        # find threshold
        depth_init = np.argmax(max_ct > 850) + win

        # define decay curve
        decaylen = pet_total.shape[1]
        decay_curve = np.concatenate((np.ones(depth_init), np.exp(-(np.arange(decaylen - depth_init)/64)/tau)), axis = 0)

        # apply curve to matrix
        rotate_slice = np.squeeze(pet_total[:,:,:,i])
        print(rotate_slice.shape)

        #depth_weighting = np.multiply(decay_curve[:, np.newaxis], pet_total[i,:,:,:])
        depth_weighting = rotate_slice * decay_curve[:, np.newaxis]

        mip_slice = np.sum(depth_weighting, axis = 1)

        total_mip[i,:,:] = mip_slice
    
    print(total_mip.shape)

    return total_mip

def define_weight_mip(pet_data, n_angles, tau : float = 0.5):
    '''
    Function to create depth-weight profile based off of defined tau constant

    Inputs
    ---------
    pet_data : data matrix (128 x 128 x n_slices x n_angles)
    tau : float value to control the depth weighting curve

    Returns:
    ---------
    mip_slice = 128 x n_slices x n_angles (2D image for each rotation)
    '''

    # process pet_data
    pet_total = rotate_interp(pet_data, n_angles, False)

    n_angles = pet_total.shape[3]
    n_slices = pet_total.shape[0]

    total_mip = np.zeros((n_angles, n_slices, pet_total.shape[1]))

    for i in range(n_angles):
            
        # get length of decay curve and define
        decaylen = pet_total.shape[1]
        decay_curve = np.exp(-(np.arange(decaylen)/64)/tau)

        # apply curve to matrix
        rotate_slice = np.squeeze(pet_total[:,:,:,i])
        depth_weighting = rotate_slice * decay_curve[:, np.newaxis]

        # sum to get mip
        mip_slice = np.sum(depth_weighting, axis = 1)

        total_mip[i,:,:] = mip_slice

    return total_mip


if __name__ == "__main__":

    nslice = 299
    n_angles = 5

    ct_data = load("C:/Users/shrut/Desktop/shrut_data/school/pet-data.tar/pet-data/data/human-scan/allslices.ct", (nslice,512,512))
    pet_data = load("C:/Users/shrut/Desktop/shrut_data/school/pet-data.tar/pet-data/data/human-scan/allslices.pet", (nslice,128,128))

    time_init = time.time()
    
    print(time.time() - time_init)

    woct_mip = define_weight_mip(pet_data, n_angles, 0.8)
    wct_mip = ct_weight_mip(pet_data, ct_data, n_angles, 0.8)

    print(woct_mip.shape)
    print(wct_mip.shape)
    
    plt.figure()
    plt.imshow(np.squeeze(woct_mip[2,:,:]))
    plt.colorbar()
    #plt.clim(0,1)
    plt.title("No CT weighting")
    plt.show()
    
    plt.figure()
    plt.imshow(np.squeeze(wct_mip[2,:,:]))
    plt.colorbar()
    #plt.clim(0,1)
    plt.title("CT weighting")
    plt.show()