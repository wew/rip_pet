import tkinter as tk
import UIElements
import GraphUI
import ripio as rio
import os
import numpy as np
import depth_weight as dw

root = tk.Tk()
root.title("RIP - PET rendering - GUI")
screenheight = root.winfo_screenheight()

def __readdata():
    sizes = [UIElements.__CT_SIZE__, UIElements.__PET_SIZE__]
    latele = [512, 128]
    plots = [petplot, ctplot]
    for ind in range(2):
        if os.path.isfile(UIElements.__FILE_NAMES__[ind]):
            GraphUI.__GRAPH_I_SEL__[ind] = latele[ind]//2
            shape = (UIElements.__N_SLICE__, latele[ind], latele[ind])
            GraphUI.__GRAPH_DATA__[ind] = rio.load(UIElements.__FILE_NAMES__[ind], shape)
            plots[ind].clim = [np.min(GraphUI.__GRAPH_DATA__[ind]), np.max(GraphUI.__GRAPH_DATA__[ind])]
            plots[ind].extent = [-latele[ind]*sizes[ind][0]/20, latele[ind]*sizes[ind][0]/20, -UIElements.__N_SLICE__*sizes[ind][2]/20, UIElements.__N_SLICE__*sizes[ind][2]/20]
            plots[ind].update()
            print(GraphUI.__GRAPH_DATA__[ind].shape)

def __processmip():
    # if both files are valid, delete the make file and load the other
    print(UIElements.__FILE_NAMES__)
    latele = 128
    size = UIElements.__MIP_SIZE__

    # check if the load/make file is valid
    validload = (type(UIElements.__FILE_NAMES__[outsel.loadbtn.id]) is str) and not (UIElements.__FILE_NAMES__[outsel.loadbtn.id] == '')
    validmake = (type(UIElements.__FILE_NAMES__[outsel.makebtn.id]) is str) and not (UIElements.__FILE_NAMES__[outsel.makebtn.id] == '')
    if validload:
        shape = (UIElements.__N_SLICE__, latele, latele)
        GraphUI.__GRAPH_DATA__[mipplot.id] = rio.load(UIElements.__FILE_NAMES__[outsel.loadbtn.id], shape)
        GraphUI.__GRAPH_I_SEL__[mipplot.id] = 0
        mipplot.clim = [np.min(GraphUI.__GRAPH_DATA__[mipplot.id]), np.max(GraphUI.__GRAPH_DATA__[mipplot.id])]
        mipplot.extent = [-latele*size[0]/20, latele*size[0]/20, -UIElements.__N_SLICE__*size[1]/20, UIElements.__N_SLICE__*size[1]/20]
        mipplot.update()
    elif validmake:
        validpet = (type(UIElements.__FILE_NAMES__[filesel.ctfilebtn.id]) is str) and not (UIElements.__FILE_NAMES__[filesel.ctfilebtn.id] == '')
        validct = (type(UIElements.__FILE_NAMES__[filesel.petfilebtn.id]) is str) and not (UIElements.__FILE_NAMES__[filesel.petfilebtn.id] == '')
        if validct and validpet:
            # make sure the CT and pet images are loaded
            __readdata()
            petdata = GraphUI.__GRAPH_DATA__[1]
            ctdata = GraphUI.__GRAPH_DATA__[0]
            weighted = dw.ct_weight_mip(petdata, ctdata, UIElements.__N_PROJ__)
            GraphUI.__GRAPH_I_SEL__[mipplot.id] = 0
            GraphUI.__GRAPH_DATA__[mipplot.id] = np.transpose(weighted, axes=(1,2,0))
            mipplot.clim = [np.percentile(GraphUI.__GRAPH_DATA__[mipplot.id], 5), np.percentile(GraphUI.__GRAPH_DATA__[mipplot.id], 95)]
            mipplot.extent = [
                -128*UIElements.__MIP_SIZE__[0]/20, 
                128*UIElements.__MIP_SIZE__[0]/20, 
                -UIElements.__N_SLICE__*UIElements.__MIP_SIZE__[1]/20, 
                UIElements.__N_SLICE__*UIElements.__MIP_SIZE__[1]/20
            ]
            mipplot.update()
            rio.save(UIElements.__FILE_NAMES__[outsel.makebtn.id], weighted)
            print(GraphUI.__GRAPH_DATA__[mipplot.id].shape)
            

def __updateslider(event):
    slideval = latslide.get()
    GraphUI.__GRAPH_I_SEL__[ctplot.id] = slideval
    ctplot.update()
    GraphUI.__GRAPH_I_SEL__[petplot.id] = 4*slideval
    petplot.update()

def __updateMIPslider(event):
    thetaslide.configure(to=UIElements.__N_PROJ__-1)
    slideval = thetaslide.get()
    GraphUI.__GRAPH_I_SEL__[mipplot.id] = slideval
    mipplot.update()


# Define UI widgets
ui = tk.Frame(root)
dispsel = UIElements.DispParams(ui)
paramsel = UIElements.IOParams(ui)
filesel = UIElements.UIInputSelect(ui, __readdata)
outsel = UIElements.UIOutputSelect(ui, __processmip)

dispsel.grid(row=0, pady=10)
paramsel.grid(row=1, pady=10)
filesel.grid(row=2, pady=10)
outsel.grid(row=3, pady=10)
ui.grid(column=1, row=0, rowspan=2)

# define tomographic plot widget
tomimages = tk.Frame(root)
petplot = GraphUI.ImPlot(tomimages, (3,4), int(screenheight/15), "Elevational [cm]", "Axial [cm]", [0,1], 'gray', [0, 1, -1, 1], title='CT')
petplot.grid(column=0, row=0, padx=10)
ctplot = GraphUI.ImPlot(tomimages, (3,4), int(screenheight/15), "Elevational [cm]", "Axial [cm]", [0,1], 'inferno', [0, 1, -1, 1], title='PET')
ctplot.grid(column=1, row=0, padx=10)
latslide = tk.Scale(tomimages, from_=0, to=127, command=__updateslider)
latslide.grid(row=0, column=2)
tomimages.grid(column=0, row=0)

# Define MIP widget
mipimages = tk.Frame(root)
mipplot = GraphUI.ImPlot(mipimages, (3,4), int(screenheight/12), "Elevational [cm]", "Axial [cm]", [0,1], 'gray', [0, 1, -1, 1], title='MIP')
mipplot.grid(column=0, row=0, pady=10)
thetaslide = tk.Scale(mipimages, from_=0, to=127, command=__updateMIPslider)
thetaslide.grid(row=0, column=2)
mipimages.grid(row=1, column=0)

if __name__ == '__main__':
    root.mainloop()
